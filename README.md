# FinTOC-2020

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.  
https://creativecommons.org/licenses/by-nc-sa/4.0/

FinTOC-2020 Shared Task, Financial Document Title Detection  
http://wp.lancs.ac.uk/cfie/shared-task/

Title: UWB@FinTOC-2020 Shared Task: Financial Document Title Detection  
Authors: Tomáš Hercig, Pavel Král (tigi | pkral @ kiv.zcu.cz)  
http://nlp.kiv.zcu.cz

We share under the above mentioned license our contribution to the shared task.

The repository contains our submissions including the fixed dataset  
(for more information see the our paper - UWB@FinTOC-2020 Shared Task.pdf).

The dataset includes the fixed train and test data. 
The submissions contain only artificial depth labels (depth = 3).


Citation

Please, cite our article if you use any of the available resources.

@InProceedings{UWB2020fintoc,  
  author    = {Hercig, Tom\'{a}\v{s}  and  Kr'{a}l, Pavel},  
  title     = {{UWB@FinTOC-2020 Shared Task: Financial Document Title Detection}},  
  booktitle = {{The 1st Joint Workshop on Financial Narrative Processing and MultiLing Financial Summarisation (FNP-FNS 2020}},
  year      = {2020}  
}

